#ifndef INCLUDED_SEAT_ALG
#define INCLUDED_SEAT_ALG

#include <algorithm>
#include <random>
#include <vector>
#include <cassert>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
//#include "seat_alg.h"

// TODO: pull out the seating layout stuff size_to own class
// TODO: have the class own the graph. 

namespace seating {

struct Student {
    bool isKeen;
    bool isInPath;
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, 
                              boost::undirectedS, Student> Graph;
typedef std::pair<size_t, size_t> Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef boost::graph_traits<Graph>::vertex_iterator   VertexIt;
typedef boost::graph_traits<Graph>::adjacency_iterator AdjIt;
typedef std::vector<Vertex> Path; 



void setEdgeSeats(const std::vector<size_t>& seatingArrangement,
                  std::vector<size_t>& edgeSeat);

void determineCloseRows(size_t keeners, 
                        size_t numStudents, 
                        const std::vector<size_t>& seatingArrangement, 
			size_t& numCloseRows,
			size_t& numStudentsInCloseRows);


class SeatingAlgorithm {
	public:
      SeatingAlgorithm(const std::vector<size_t> edgeSeats);
		static bool chooseKeenerNext(size_t keenersLeft, size_t closeSeatsLeft, std::mt19937& gen); 
		void rewindPath(Graph& g, Path& p, std::mt19937& gen);
		template <typename Iterator>
			static Iterator chooseVertexIUAT(const std::vector<Iterator>& choices, std::mt19937& gen)
			{
				std::uniform_int_distribution<size_t> dist(0, choices.size()-1);
				return choices[dist(gen)];
			}
	
	        template <typename Iterator>
		static std::vector<Iterator> filterOutIneligibleVertices(const Graph& g, const Vertex& v, bool chooseAKeener)
		{
		//    typedef typename  std::vector<Iterator>;
		    return std::vector<Iterator>();

		}
	   void getChoicesForVertex(std::vector<AdjIt>& choices, Graph& g, Vertex& v, bool chooseAKeener);
		void getChoicesForFirstVertex(std::vector<VertexIt>& choices, Graph& g, bool chooseAKeener);
		std::vector<Path> calculateSeatingPlans(size_t numStudents, size_t numKeeners, size_t numCloseSeats,  size_t maxRetries);

		bool calculateSeatingPlan(Graph& g,
		              Path&  p,
			      size_t numStudents,
			      size_t numKeeners,
			      size_t numCloseSeats,
			      size_t maxRetries,
			      std::mt19937& gen)
    {

        size_t retries = 0; 
		  std::vector<VertexIt> choicesForFirstSeatInRow;
		  std::vector<AdjIt>    choicesForSeat;
		  bool chooseKeener; 

        while (p.size() < numStudents )
		  {
			  size_t numCloseSeatsLeft = numCloseSeats > p.size() ? numCloseSeats - p.size() : 0;
			  chooseKeener = chooseKeenerNext(numKeeners, numCloseSeatsLeft, gen); 
			  numKeeners -= chooseKeener ? 1 : 0;
			  std::vector<size_t>::const_iterator eIt = std::find(m_edgeSeats.begin(), m_edgeSeats.end(), p.size() );
			  if (eIt != m_edgeSeats.end())
			  {
				  // probably better to write something like
				  // auto iterators = adjacent_vertices()
				  // std::vector<AdjIt> choicesForNextSeat = filterOutVerticesAlreadyInPath( ... );
				  SeatingAlgorithm::getChoicesForFirstVertex(choicesForFirstSeatInRow, g, chooseKeener); 
				  updatePath(choicesForFirstSeatInRow, g, p, gen, retries);
			  }
			  else
			  {
				  assert(!p.empty()); 
				  Vertex lastInPath = p.back(); 
				  SeatingAlgorithm::getChoicesForVertex(choicesForSeat, g, lastInPath, chooseKeener); 
				  updatePath(choicesForSeat, g, p, gen, retries); 
			  }
			  if (retries > maxRetries)
			  {
				  std::cout << "Failed to find a path" << std::endl;
				  return false;
			  }
		  }
		  return true;
	 }

     void updateGraphAndPath(Graph& g, Path& p)
    {
        //update the edges in the graph to remove those that are in the path.
		 for(size_t i = 0; i != p.size() - 1; ++i) // TODO: fix
		 {
			 if (std::find(m_edgeSeats.begin(), m_edgeSeats.end(), i+1) == m_edgeSeats.end())
			 {
				 // remove the edge from the graph 
				 remove_edge(p[i],p[i+1], g); 
			 }
			 // update the property that the vertices are in the path
			 g[p[i]].isInPath = false;
			 g[p[i+1]].isInPath = false;
		 }
	 }

    bool isValidSeatingPlan(const Graph& g, const Path& p)
    {
		 for(size_t i = 0; i != p.size() - 1 ; ++i)  // TODO:
		 {
			 if (std::find(m_edgeSeats.begin(), m_edgeSeats.end(), i+1) != m_edgeSeats.end())
			 {
				 continue;
			 }
			 auto edgePair = boost::edge(p[i], p[i+1], g);
			 if (!edgePair.second)
				 return false;
		 }
		 return true;
	 }


    Vertex chooseBackupVertex(std::vector<AdjIt>& backupChoices, const Path& p, std::mt19937& gen)
    {
        assert(!p.empty()); 
		  if (backupChoices.empty())
		  {
			  return p.front(); 
		  }
		  else
		  {
			  std::uniform_int_distribution<size_t> dist(0, backupChoices.size() - 1 );
			  return *(backupChoices[dist(gen)]);
		  }
	 }

    template <typename Iterator>
    void updatePath(std::vector<Iterator>& choices, Graph& g, Path& p, std::mt19937& gen, size_t& retries )
    {
		 if (choices.empty())
		 {
			 rewindPath(g, p, gen); 
			 retries++;
		 }
		 else
		 {
			 Iterator nextVertex = chooseVertexIUAT(choices, gen);
			 p.push_back(*nextVertex);
			 g[*nextVertex].isInPath = true;
	 }
    }

    virtual ~SeatingAlgorithm() {}

private:
	Graph m_g;
   std::vector<size_t> m_edgeSeats;
		
			      

}; 



} // closes namespace seating


#endif 
