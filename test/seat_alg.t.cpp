#include "gtest/gtest.h"
#include "../seat_alg.h"
#include <random>

struct GraphTest : public testing::Test
{
    seating::Graph g;
    seating::Path p;
    std::vector<int> edgeSeats; 
    GraphTest() : g(), p() 
    {
        // put some vertices in. 
	add_edge(0,1, g);
	add_edge(0,2, g);
	add_edge(1,2, g);
	add_edge(1,4, g);
	add_edge(2,3, g); 
    }
};

TEST_F(GraphTest, testRewindOnEmptyPath)
{
    using namespace seating;
    std::mt19937 gen;  
    SeatingAlgorithm::rewindPath(g,p, gen); 
    EXPECT_EQ(p.size(), 0); 
}

TEST_F(GraphTest, ensurePathRewindsCorrectlyToOneVertex)
{
    p.push_back(2);
    g[2].isInPath = true;
    p.push_back(1);
    g[1].isInPath = true;
    p.push_back(4);
    g[4].isInPath = true;
    using namespace seating;
    std::mt19937 gen;  
    SeatingAlgorithm::rewindPath(g,p, gen); 
    SeatingAlgorithm::printPath(p); 
    EXPECT_FALSE(p.empty());
    EXPECT_EQ(p.back(), 2); 

}

TEST_F(GraphTest, checkAdjSizeIsCorrect)
{
    using namespace seating;
    auto adjIters = adjacent_vertices(0, g);
    EXPECT_EQ(adjIters.second - adjIters.first, 2); 
    std::pair< AdjIt, AdjIt> it = adjacent_vertices(1, g); 
    EXPECT_EQ(it.second - it.first, 3); 
}

TEST_F(GraphTest, testUpdatingGraphWithOnlyEdgeSeatsDoesNothing)
{
    using namespace seating;
    p.push_back(0);
    p.push_back(1);
    p.push_back(4);
    edgeSeats.push_back(0);
    edgeSeats.push_back(1);
    edgeSeats.push_back(2);
    EXPECT_EQ(edge(0,1,g).second, true); 
    EXPECT_EQ(edge(1,4,g).second, true); 
    seating::SeatingAlgorithm::updateGraphAndPath(g,p,edgeSeats); 
    EXPECT_EQ(edge(0,1,g).second, true); 
    EXPECT_EQ(edge(1,4,g).second, true); 
}

TEST_F(GraphTest, testGraphIsUpdatedProperly)
{
    using namespace seating;
    p.push_back(0);
    p.push_back(1);
    p.push_back(3);
    EXPECT_EQ(edge(0,1,g).second, true); 
    seating::SeatingAlgorithm::updateGraphAndPath(g,p,edgeSeats); 
    EXPECT_EQ(edge(0,1,g).second, false); 
    EXPECT_EQ(g[0].isInPath, false);
    EXPECT_EQ(g[1].isInPath, false);
}


TEST_F(GraphTest, testLinksBetweenRowsNotCheckedForValidity)
{
    using namespace seating;
    p.push_back(0);
    p.push_back(1);
    p.push_back(3);
    edgeSeats.push_back(2);
    EXPECT_EQ(seating::SeatingAlgorithm::isValidSeatingPlan(g,p, edgeSeats), true);
}
 

TEST_F(GraphTest, testValidPathIsValid)
{
    using namespace seating;
    p.push_back(0);
    p.push_back(1);
    EXPECT_EQ(seating::SeatingAlgorithm::isValidSeatingPlan(g,p, edgeSeats), true);
}

TEST_F(GraphTest, testInvalidPathMarkedInvalid)
{
    using namespace seating;
    p.push_back(0);
    p.push_back(1);
    p.push_back(3);
    EXPECT_EQ(seating::SeatingAlgorithm::isValidSeatingPlan(g,p, edgeSeats), false);

}

TEST(chooseKeener, testOnlyKeenersLeftReturnsTrue)
{
    std::mt19937 gen;  
    EXPECT_EQ(seating::SeatingAlgorithm::chooseKeenerNext(4,4, gen), true); 
}

TEST(chooseKeener, testNoKeenersLeftReturnsFalse)
{
    std::mt19937 gen;
    EXPECT_EQ(seating::SeatingAlgorithm::chooseKeenerNext(0,4, gen), false); 
    EXPECT_EQ(seating::SeatingAlgorithm::chooseKeenerNext(0,0, gen), false); 
}

TEST(determineCloseRows, checkOneRowExactlyFills)
{
    int keeners = 4;
    int numStudents = 6;
    std::vector<int> seatingArrangement = {4, 4, 4,4 };
    int numCloseRows;
    int numStudentsInCloseRows;
    seating::determineCloseRows(keeners, numStudents,  seatingArrangement, numCloseRows, numStudentsInCloseRows); 
    EXPECT_EQ(1, numCloseRows);
    EXPECT_EQ(4, numStudentsInCloseRows); 

}

TEST(determineCloseRows, checkOneUnderExactFill)
{
    int keeners = 3;
    int numStudents = 6;
    std::vector<int> seatingArrangement = {4, 4, 4,4 };
    int numCloseRows, numStudentsInCloseRows;
    seating::determineCloseRows(keeners, numStudents,  seatingArrangement, numCloseRows, numStudentsInCloseRows); 
    EXPECT_EQ(1, numCloseRows);
    EXPECT_EQ(4, numStudentsInCloseRows); 
}

TEST(determineCloseRows, noKeeners)
{
    int keeners = 0;
    int numStudents = 6;
    std::vector<int> seatingArrangement = {4, 4, 4,4 };
    int numCloseRows, numStudentsInCloseRows;
    seating::determineCloseRows(keeners, numStudents,  seatingArrangement, numCloseRows, numStudentsInCloseRows); 
    EXPECT_EQ(0, numCloseRows);
    EXPECT_EQ(0, numStudentsInCloseRows); 
}

TEST(determineCloseRows, testNumStudentsInCloseRowsLessThanTotalStudents)
{
    int keeners = 6,  numStudents = 6;
    std::vector<int> seatingArrangement = {4, 4, 4,4 };
    int numCloseRows, numStudentsInCloseRows;
    seating::determineCloseRows(keeners, numStudents,  seatingArrangement, numCloseRows, numStudentsInCloseRows); 
    EXPECT_EQ(2, numCloseRows);
    EXPECT_EQ(6, numStudentsInCloseRows); 
}

TEST(setEdgeSeats, checkEdgeSeatsAreSetCorrectly)
{
    std::vector<int> seatingArrangement = {4, 1, 3, 5};
    std::vector<int> edgeSeats; 
    seating::setEdgeSeats(seatingArrangement, edgeSeats);
    EXPECT_NE(std::find(edgeSeats.begin(), edgeSeats.end(), 0), edgeSeats.end());
    EXPECT_NE(std::find(edgeSeats.begin(), edgeSeats.end(), 4), edgeSeats.end());
    EXPECT_NE(std::find(edgeSeats.begin(), edgeSeats.end(), 5), edgeSeats.end());
    EXPECT_NE(std::find(edgeSeats.begin(), edgeSeats.end(), 8), edgeSeats.end());
}

struct UpdatePathTest : testing::Test {
    seating::Graph g;
    seating::Path p;
    std::mt19937 gen;  
    
    UpdatePathTest() : g(), p(), gen()
    {
        // put some vertices in. 
	add_edge(0,1, g);
	add_edge(0,2, g);
	add_edge(1,2, g);
	add_edge(1,4, g);
	add_edge(2,3, g); 

	p.push_back(1); 
	p.push_back(4);
    }

};


