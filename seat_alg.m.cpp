#include <iostream>
#include <fstream>
#include <utility>
#include <algorithm>
#include <random>
#include <vector>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include "seat_layout.h"
#include "seat.h"
#include "seat_alg.h"

#include "json.hpp"


int main(int argc, char *argv[])
{
    using namespace boost; 
    using namespace seating; 


    size_t numStudents =10;
    size_t numKeeners = 0;
    
//    std::vector<int> seatingArrangement = { 2, 2, 2, 2, 2}; 
    std::vector<size_t> seatingArrangement = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}; 

    size_t maxRetries = 100;
	 size_t numCloseRows = 0, numCloseSeats = 0;
	 determineCloseRows(numKeeners, numStudents, seatingArrangement, numCloseRows, numCloseSeats);
	 std::vector<size_t> edgeSeats; 
	 setEdgeSeats(seatingArrangement, edgeSeats); 

    SeatingAlgorithm seatAlg(edgeSeats);
    std::vector<Path> paths = seatAlg.calculateSeatingPlans(numStudents, numKeeners, numCloseSeats, maxRetries); 
    std::for_each(paths.begin(), paths.end(), [](const Path& p){ 
                  std::for_each(p.begin(), p.end(), [](const Vertex& v) { std::cout << v << " "; });
		  std::cout << std::endl;
		  }); 
    std::cout << "The number of paths found is = " << paths.size() << std::endl;

    return 0;
}

