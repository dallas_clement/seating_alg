OBJS = seat_alg.o seat_alg.m.o
CC   = g++ 
DEBUG = -g 
CFLAGS = -Wall -c -std=gnu++11 $(DEBUG)
LFLAGS = -Wall $(DEBUG)

p1 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o pGraphAlg

seat_alg.m.o : seat_alg.h seat_alg.m.cpp
	$(CC) $(CFLAGS) seat_alg.m.cpp

seat_alg.o : seat_alg.h 
	$(CC) $(CFLAGS) seat_alg.cpp
#seat_layout.h seat.h

#seat_layout.o : seat_layout.h seat_layout.cpp seat.h
#	$(CC) $(CFLAGS) seat_layout.cpp

clean : 
	\rm *.o  pGraphAlg
