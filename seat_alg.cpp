#include "seat_alg.h"

#include <iostream>
#include <utility>
#include <algorithm>
#include <random>
#include <vector>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>

using namespace boost;
namespace  seating {
		
void printPath(Path& p)
{
	std::cout << "Path is: ";
	for_each(p.begin(), p.end(), [](Vertex& v) { std::cout << v << " , "; });
	std::cout << std::endl; 
}

void printGraph(const Graph& g)
{
	std::cout << "Graph is "; 
	auto it = edges(g);
	for(; it.first != it.second; ++it.first)
	{
		std::cout << *it.first << " , "; 
	}
	std::cout << std::endl; 
}


bool SeatingAlgorithm::chooseKeenerNext(size_t keenersLeft, size_t closeSeatsLeft, std::mt19937& gen)
{
    if (keenersLeft == 0)
        return false; 
    std::uniform_int_distribution<size_t> dist(1, closeSeatsLeft); 
    if (dist(gen) > keenersLeft)
        return false;
    else
        return true;
}

void setEdgeSeats(const std::vector<size_t>& seatingArrangement,
                  std::vector<size_t>& edgeSeat)
{
    size_t currSeatNumber = 0; 
    for(std::vector<size_t>::const_iterator cit = seatingArrangement.begin();
           cit != seatingArrangement.end(); ++cit)
    {
        edgeSeat.push_back(currSeatNumber);
	currSeatNumber += *cit;
    }
}

//  should just return number of close seats
void determineCloseRows(size_t numKeeners, 
                        size_t numStudents,
                        const std::vector<size_t>& seatingArrangement, 
			size_t& numCloseRows,
			size_t& numStudentsInCloseRows)
{
    numCloseRows = 0;
    numStudentsInCloseRows = 0; 
    for(size_t i = 0; i != seatingArrangement.size(); ++i)
    {
        if ( numStudentsInCloseRows >= numKeeners )
	{
	   break;
	}
	else
	{
	    numCloseRows++;
	    numStudentsInCloseRows += seatingArrangement[i];
	}
    }
    numStudentsInCloseRows = std::min(numStudentsInCloseRows, numStudents); 
}

SeatingAlgorithm::SeatingAlgorithm(const std::vector<size_t> edgeSeats) : m_edgeSeats(edgeSeats) {}

void SeatingAlgorithm::rewindPath(Graph& g, Path& p, std::mt19937& gen)
{
    if ( 0 == p.size() ) 
        return; 
        
    auto adjIters = adjacent_vertices(p.back(), g);
    std::vector<AdjIt> backupChoices;
    for(; adjIters.first != adjIters.second; ++adjIters.first)
    { 
        if (g[*adjIters.first].isInPath)
	    backupChoices.push_back(adjIters.first);
    }

    Vertex backup = chooseBackupVertex(backupChoices, p, gen); 
    Path::iterator it = std::find(p.begin(), p.end(), backup);

    for(Path::const_iterator s = it; s != p.end(); ++s)
    {
        g[*s].isInPath = false;
    }
    p.erase(it, p.end());
}

void SeatingAlgorithm::getChoicesForVertex(std::vector<AdjIt>& choices, Graph& g, Vertex& v, bool chooseAKeener)
{
	choices.clear(); 
	auto vItPair = adjacent_vertices(v, g);
	auto it = vItPair.first;
	auto end = vItPair.second;
	for(; it != end; ++it)
	{
		if ( (g[*it].isKeen == chooseAKeener) && !g[*it].isInPath)
		{
			choices.push_back(it);
		}
	}
}

void SeatingAlgorithm::getChoicesForFirstVertex(std::vector<VertexIt>& choices, Graph& g, bool chooseAKeener)
{
	choices.clear();
	auto vItPair = vertices(g);
	auto it = vItPair.first;
	auto end = vItPair.second;
	for(; it != end; ++it)
	{
		if ( (g[*it].isKeen == chooseAKeener) && !g[*it].isInPath)
		{
			choices.push_back(it);
		}
	}
}

std::vector<Path> SeatingAlgorithm::calculateSeatingPlans(size_t numStudents, size_t numKeeners, size_t numCloseSeats, size_t maxRetries)
		{
		 Graph g(numStudents);
		 for(size_t i = 1; i != numStudents; ++i)
		 {
			 for(size_t j = 0; j != i; ++j)
			 {
				 add_edge(j, i, g);
			 }
		 }
		 std::random_device rd; 
		 std::mt19937 gen(rd()); 
		
		 std::vector<Path> paths;
		 while (size_t(paths.size()) < numStudents) 
		 {
			 Path path;
			 bool foundPlan = SeatingAlgorithm::calculateSeatingPlan(g, path, numStudents, numKeeners, numCloseSeats, maxRetries, gen); 
			 if (foundPlan)
			 {
				 paths.push_back(path);
				 if (!isValidSeatingPlan(g,path))
				 {
					 printPath(path);
					 printGraph(g);
				 }
				 assert(isValidSeatingPlan(g,path));
				 SeatingAlgorithm::updateGraphAndPath(g, path); 

				 printGraph(g);
				 path.clear(); 
			 }
			 else
			 {
				 break;
			 }
		 }
		 return paths;
}





} // closes namespace seating




